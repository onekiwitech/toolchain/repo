# repo

- `mkdir ~/bin`
- `curl https://storage.googleapis.com/git-repo-downloads/repo-1 > ~/bin/repo`
- `chmod a+x ~/bin/repo`
- `rm -rf ~/.repoconfig` (remove old config)
- add to **.bashrc** `export PATH=$PATH:~/bin`
- `source ~/.bashrc`
